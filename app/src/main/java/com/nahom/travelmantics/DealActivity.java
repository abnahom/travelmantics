package com.nahom.travelmantics;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

public class DealActivity extends AppCompatActivity {
    private FirebaseDatabase mdb;
    private DatabaseReference dbRef;
    private static final int PICTURE_RESULT = 42;
    EditText txtTitle;
    EditText txtDescription;
    EditText txtPrice;
    ImageView imageView;
    TravelDeal deal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.deal_detail);
        getSupportActionBar().setTitle("Travel Deal");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        mdb = FirebaseUtil.db;
        dbRef = FirebaseUtil.reference;
        txtTitle = (EditText) findViewById(R.id.text_title);
        txtDescription = (EditText) findViewById(R.id.text_description);
        txtPrice = (EditText) findViewById(R.id.text_price);
        imageView = (ImageView) findViewById(R.id.image_deal);
        // Fish the below out for a better UI of Detail View ie. uneditable View for Vistors
        Intent intent = getIntent();
        TravelDeal deal = (TravelDeal) intent.getSerializableExtra("Deal");
        if (deal == null){
            deal = new TravelDeal();
        }
        this.deal = deal;
        txtTitle.setText(deal.getTitle());
        txtDescription.setText(deal.getDescription());
        if (FirebaseUtil.isAdmin){
           txtPrice.setText(deal.getPrice());
        }else{
            formatPriceToCurrency(deal.getPrice());
        }
        showImage(deal.getImageUrl());
        Button btnImage = (Button) findViewById(R.id.btn_image);
        if (deal.getImageUrl() != null){
            btnImage.setText("Change Image");
        }
        //TODO request for permission to access storage at run time.
        btnImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent imgIntent = new Intent(Intent.ACTION_GET_CONTENT);
                imgIntent.setType("image/jpeg");
                imgIntent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
                startActivityForResult(imgIntent.createChooser(imgIntent,"Insert Picture"),PICTURE_RESULT);
            }
        });
        // Upto this Line
}
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.save_menu:
                saveDeal();
                Toast.makeText(this,"Deal Saved Successfully.",Toast.LENGTH_LONG).show();
                clean();
                backToList();
                return true;
            case R.id.delete_menu:
                deleteDeal();

                return true;

            default:
                    return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.save_menu,menu);
        if(FirebaseUtil.isAdmin){
            menu.findItem(R.id.delete_menu).setVisible(true);
            menu.findItem(R.id.save_menu).setVisible(true);
            enableEdit(true);
            findViewById(R.id.btn_image).setVisibility(View.VISIBLE);
            findViewById(R.id.btn_image).setEnabled(true);
        }else{
            menu.findItem(R.id.delete_menu).setVisible(false);
            menu.findItem(R.id.save_menu).setVisible(false);
            enableEdit(false);
            findViewById(R.id.btn_image).setVisibility(View.INVISIBLE);
            findViewById(R.id.btn_image).setEnabled(false);
        }
        return  true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICTURE_RESULT && resultCode == RESULT_OK){
            Uri imageUri = data != null ? data.getData() : null;
            if (imageUri != null) {
                final ProgressDialog progressDialog = new ProgressDialog(this);
                progressDialog.setTitle("Uploading...");
                progressDialog.show();
                progressDialog.setCancelable(false);
                final StorageReference ref = FirebaseUtil.storageRef.child(imageUri.getLastPathSegment());
                ref.putFile(imageUri).addOnSuccessListener(this, new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        final String imageName = taskSnapshot.getStorage().getPath();
                        ref.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                            @Override
                            public void onSuccess(Uri uri) {
                                String Uri = uri.toString();
                                Log.d("Travel","Download Url = "+ Uri);
                                Log.d("Travel","Image Name = "+ imageName);

                                deal.setImageUrl(Uri);
                                deal.setImageName(imageName);
                                showImage(Uri);
                                progressDialog.dismiss();
                            }
                        });
                    }
                }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                        double progress = (100.0*taskSnapshot.getBytesTransferred()/taskSnapshot
                                .getTotalByteCount());
                        progressDialog.setMessage("Uploaded "+(int)progress+"%");
                    }
                });
            }
        }
    }

    private void clean() {
        txtTitle.setText("");
        txtDescription.setText("");
        txtPrice.setText("");
        txtTitle.requestFocus();
    }

    private void saveDeal() {
        deal.setTitle(txtTitle.getText().toString());
        deal.setDescription(txtDescription.getText().toString());
        deal.setPrice(txtPrice.getText().toString());
            if (deal.getId() == null){
                dbRef.push().setValue(deal);
            }else{
                dbRef.child(deal.getId()).setValue(deal);
            }
    }
    private void deleteDeal(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.AlertDialogTheme);
        builder.setMessage(R.string.delete_warning);
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (deal.getId() ==null){
                    Toast.makeText(DealActivity.this,"Please save the deal before deleting!",Toast.LENGTH_SHORT).show();
                    return;
                }
                dbRef.child(deal.getId()).removeValue();
                if (deal.getImageName() != null && !deal.getImageName().isEmpty()){
                    StorageReference picRef = FirebaseUtil.storage.getReference().child(deal.getImageName());
                    picRef.delete().addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            Log.d("Travel", "Image Deleted Successfully!");
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Log.d("Travel",e.getMessage());
                        }
                    });
                }
                Toast.makeText(DealActivity.this,"Deal Deleted Successfully.",Toast.LENGTH_LONG).show();
                backToList();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(DealActivity.this,"Okay, Bye!",Toast.LENGTH_LONG).show();
            }
        });
        final AlertDialog dialog = builder.create();
        dialog.show();
//        TextView msgTxt = (TextView) dialog.findViewById(android.R.id.message);
//        msgTxt.setTextSize(18);
    }
    private void backToList(){
        Intent intent = new Intent(this,ListActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    private void enableEdit(boolean isEnabled){
        txtTitle.setEnabled(isEnabled);
        txtDescription.setEnabled(isEnabled);
        txtPrice.setEnabled(isEnabled);
    }

    private void showImage(String Url){
        if (Url != null && !Url.isEmpty()){
            int width = Resources.getSystem().getDisplayMetrics().widthPixels;
            Picasso.get()
                    .load(Url)
                    .placeholder(R.drawable.placeholder_pic)
                    .error(R.drawable.placeholder_pic)
                    .resize(width,width*2/3)
                    .centerCrop()
                    .into(imageView);
        }
    }

    private void formatPriceToCurrency(String price) {
        int priceToInt = 0;
        DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        symbols.setGroupingSeparator(',');
        symbols.setDecimalSeparator('.');
        DecimalFormat decimalFormat = new DecimalFormat("ETB #,###.00", symbols);
        try {
            priceToInt = Integer.parseInt(price);
        } catch(NumberFormatException nfe) {
            System.out.println("Could not parse " + nfe);
        }
        String formatedPrice = decimalFormat.format(priceToInt);
        Log.d("Travel",formatedPrice);
        txtPrice.setText(formatedPrice);
    }


}
