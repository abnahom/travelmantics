package com.nahom.travelmantics;

import android.app.Activity;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.firebase.ui.auth.AuthUI;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FirebaseUtil {
    private static final int RC_SIGN_IN = 123 ;
    public static FirebaseDatabase db;
    public static DatabaseReference reference;
    public static FirebaseStorage storage;
    public static StorageReference storageRef;
    private static FirebaseUtil firebaseUtil;
    public static FirebaseAuth auth;
    public static FirebaseAuth.AuthStateListener authStateListener;
    public static ArrayList<TravelDeal> mdeals;
    public static boolean isAdmin;
    private static ListActivity caller;
    private FirebaseUtil(){
    }

    public static void openFbReference(String ref, final ListActivity callerActivity){
        if (firebaseUtil  == null) {
            firebaseUtil = new FirebaseUtil();
                db = FirebaseDatabase.getInstance();
                auth = FirebaseAuth.getInstance();
                caller = callerActivity;
                authStateListener = new FirebaseAuth.AuthStateListener() {
                    @Override
                    public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                        if (firebaseAuth.getCurrentUser() != null) {
                            String user = firebaseAuth.getUid();
                            checkIfAdmin(user);
//                            Toast.makeText(callerActivity.getBaseContext(),"Welcome Back",Toast.LENGTH_LONG).show();
                        } else {
                            FirebaseUtil.signIn();
                        }
                    }
                };
                connectStorage();
        }
        mdeals = new ArrayList<TravelDeal>();
        reference = db.getReference().child(ref);
    }

    private static void checkIfAdmin(String uid) {
        FirebaseUtil.isAdmin = false;
        DatabaseReference ref = db.getReference().child("administrators").child(uid);
        ChildEventListener listener = new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                FirebaseUtil.isAdmin = true;
                caller.showMenu();
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        };
        ref.addChildEventListener(listener);
    }

    private static void signIn(){
        // Choose authentication providers
        List<AuthUI.IdpConfig> providers = Arrays.asList(
                new AuthUI.IdpConfig.EmailBuilder().build(),
                new AuthUI.IdpConfig.GoogleBuilder().build());

        // Create and launch sign-in intent
        caller.startActivityForResult(
                AuthUI.getInstance()
                        .createSignInIntentBuilder()
                        .setAvailableProviders(providers)
                        .setLogo(R.drawable.icon)
                        .build(), RC_SIGN_IN);
    }


    public static void attachListener(){
        auth.addAuthStateListener(authStateListener);
    }
    public static void detachListener(){
        auth.removeAuthStateListener(authStateListener);
    }

    public static void connectStorage (){
        storage = FirebaseStorage.getInstance();
        storageRef = storage.getReference().child("deals_pictures");
    }
}
