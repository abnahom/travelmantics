package com.nahom.travelmantics;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

public class DealAdapter extends RecyclerView.Adapter<DealAdapter.DealViewHolder>{
    ArrayList<TravelDeal> deals;
    private FirebaseDatabase db;
    private DatabaseReference dbRef;
    private ChildEventListener childEventListener;
    private ImageView imageThumbnail;

    public DealAdapter(){

//        FirebaseUtil.openFbReference("traveldeals",null);
        db = FirebaseUtil.db;
        dbRef = FirebaseUtil.reference;
        this.deals = FirebaseUtil.mdeals;
        childEventListener = new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                TravelDeal travelDeal = dataSnapshot.getValue(TravelDeal.class);
                if (travelDeal != null) {
                    travelDeal.setId(dataSnapshot.getKey());
                }
                deals.add(travelDeal);
                notifyItemInserted(deals.size()-1);

            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        };
        dbRef.addChildEventListener(childEventListener);
    }
    @NonNull
    @Override
    public DealViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        View itemView = LayoutInflater.from(context).inflate(R.layout.rv_card_item, parent,false);
        return new DealViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull DealViewHolder holder, int position) {
        TravelDeal deal = deals.get(position);
        holder.bind(deal);

    }

    @Override
    public int getItemCount() {
        return deals.size();
    }

    public class DealViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        TextView dealTitle;
        TextView dealDescription;
        TextView dealPrice;
        Button readMoreButton;
        public DealViewHolder(@NonNull View itemView) {
            super(itemView);
            dealTitle = (TextView) itemView.findViewById(R.id.deal_title);
            dealDescription = (TextView) itemView.findViewById(R.id.deal_description);
            dealPrice = (TextView) itemView.findViewById(R.id.deal_price);
            imageThumbnail = (ImageView) itemView.findViewById(R.id.deal_thumbnail);
            readMoreButton = (Button) itemView.findViewById(R.id.read_more);
            if (FirebaseUtil.isAdmin){
                readMoreButton.setText("Edit");
            }else {
                readMoreButton.setText("Read More");
            }
            readMoreButton.setOnClickListener(this);
        }
        public void bind(TravelDeal deal){
            imageThumbnail.setClipToOutline(true);
            dealTitle.setText(deal.getTitle());
            dealDescription.setText(deal.getDescription());
            formatPriceToCurrency(deal.getPrice());
            if (deal.getImageUrl() == null){
                imageThumbnail.setImageResource(R.drawable.placeholder_pic);
            }else{
                showImage(deal.getImageUrl());
            }
        }

        private void formatPriceToCurrency(String price) {
            int priceToInt = 0;
            DecimalFormatSymbols symbols = new DecimalFormatSymbols();
            symbols.setGroupingSeparator(',');
            symbols.setDecimalSeparator('.');
            DecimalFormat decimalFormat = new DecimalFormat("ETB #,###.00", symbols);
            try {
                priceToInt = Integer.parseInt(price);
            } catch(NumberFormatException nfe) {
                System.out.println("Could not parse " + nfe);
            }
            String formatedPrice = decimalFormat.format(priceToInt);
            Log.d("Travel",formatedPrice);
            dealPrice.setText(formatedPrice);
        }

        @Override
        public void onClick(View view) {
            Context context = view.getContext();
            int position = getAdapterPosition();
            TravelDeal selectedDeal = deals.get(position);
            Intent intent = new Intent(context,DealActivity.class);
            intent.putExtra("Deal",selectedDeal);
            context.startActivity(intent);
        }
        private void showImage(String Url){
            if (Url != null && !Url.isEmpty()){
                int width = Resources.getSystem().getDisplayMetrics().widthPixels;
                Picasso.get()
                        .load(Url)
                        .placeholder(R.drawable.placeholder_pic)
                        .resize(width,width*2/3)
                        .centerCrop()
                        .error(R.drawable.placeholder_pic)
                        .into(imageThumbnail);
            }
        }
    }
}
